package com.example.myapplication

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    internal var score = 0
    internal var gameStarted = false
    internal lateinit var countDownTimer: CountDownTimer
    internal val initialCoutDown: Long = 60000
    internal val countDownInt: Long = 1000
    internal var timeLeftOnTimer: Long = 60000
    internal lateinit var btTap: Button
    internal lateinit var txtGamescore: TextView
    internal lateinit var txtTime: TextView

    companion object {
        private val TAG = MainActivity::class.java.simpleName
        private const val SCORE_KEY = "SCORE_KEY"
        private const val TIME_LEFT_KEY = "TIME_LEFT_KEY"
    }
    //
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //for logcat message
        Log.d(TAG, "onCreate called. Score is : $score")
        btTap = findViewById(R.id.btTap)
        txtGamescore = findViewById(R.id.txtGamescore)
        txtTime = findViewById(R.id.txtTime)
        btTap.setOnClickListener { view ->
            val bounceAnimation = AnimationUtils.loadAnimation(this, R.anim.bounce)
            view.startAnimation(bounceAnimation)
            incrementScore()
        }
        //If savedInstanceState is null, call resetGame, else call restoreGame
        if (savedInstanceState != null) {
            score = savedInstanceState.getInt(SCORE_KEY)
            timeLeftOnTimer = savedInstanceState.getLong((TIME_LEFT_KEY))
            restoreGame()
        } else {
            resetGame()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.actionAbout) {
            showInfo()
        }
        return true
    }

    @SuppressLint("StringFormatInvalid")
    private fun showInfo() {
        val dialogTitle = getString(R.string.aboutTitle, BuildConfig.VERSION_NAME)
        val dialogMessage = getString(R.string.aboutMessage)
        val builder = AlertDialog.Builder(this)
        builder.setTitle(dialogTitle)
        builder.setMessage(dialogMessage)
        builder.create().show()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(SCORE_KEY, score)
        outState.putLong(TIME_LEFT_KEY, timeLeftOnTimer)
        countDownTimer.cancel()
        Log.d(TAG, "onSaveInstanceState: Saving score: $score & Time left: $timeLeftOnTimer")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy called")
    }

    private fun resetGame() {
        //reset score to 0
        score = 0
        txtGamescore.text = getString(R.string.txtGamescore, score)
        val initialTimeLeft = initialCoutDown / 1000
        txtTime.text = getString(R.string.timeLeft, initialTimeLeft)
        countDownTimer = object : CountDownTimer(initialCoutDown, countDownInt) {
            override fun onTick(millisUntilFinished: Long) {
                timeLeftOnTimer = millisUntilFinished
                val timeLeft = millisUntilFinished / 1000
                txtTime.text = getString(R.string.timeLeft,timeLeft)
            }

            override fun onFinish() {
                endGame()
            }
        }
        gameStarted = false
    }
    private fun restoreGame() {
        //Use values restored via the passed in savedInstanceState instead of default value
        txtGamescore.text = getString(R.string.txtGamescore, score)
        val restoredTime = timeLeftOnTimer / 1000
        txtTime.text = getString(R.string.timeLeft, restoredTime)
        countDownTimer = object : CountDownTimer(timeLeftOnTimer, countDownInt) {
            override fun onTick(millisUntilFinished: Long) {
                timeLeftOnTimer = millisUntilFinished
                val timeLeft = millisUntilFinished / 1000
                txtTime.text = getString(R.string.timeLeft, timeLeft)
            }

            override fun onFinish() {
                endGame()
            }
        }
        countDownTimer.start()
        gameStarted = true
    }
    private fun incrementScore() {
        if (!gameStarted)
            startGame()
        score += 1
        val newScore = getString(R.string.txtGamescore, score)
        txtGamescore.text = newScore
        val blinkAnimation = AnimationUtils.loadAnimation(this, R.anim.blink)
        txtGamescore.startAnimation(blinkAnimation)
    }

    private fun startGame() {
        countDownTimer.start()
        gameStarted = true
    }

    private fun endGame() {
        Toast.makeText(this, getString(R.string.txtGameOver, score), Toast.LENGTH_LONG).show()
        resetGame()
    }
}