fun main(){
//    val videoGamesCollection = mutableMapOf<String, MutableList<String>>()
//    println(videoGamesCollection)
//
//    videoGamesCollection.put("Action", mutableListOf("Dark Souls","GTAV"))
//    println(videoGamesCollection)
//
//    val actionGames = videoGamesCollection["Action"]
//    println(actionGames)
//
//    videoGamesCollection["Strategy"] = mutableListOf("AoE","Stars Craft")
//
//    println(videoGamesCollection)
//    val stragtegyGames = videoGamesCollection["Strategy"]
//    println(stragtegyGames)
//
//    val authenticatorHeader = mapOf(
//        "API_KEY" to "your-api-key",
//        "Authorization" to "auth token",
//        "content/type" to "application/json")
//
//    println(authenticatorHeader)
//    videoGamesCollection["Strategy"]?.add("Civilization VI")
//    println(videoGamesCollection)
//
//    val removedActionGames = videoGamesCollection.remove("Action")
//    println(removedActionGames)
//    println(videoGamesCollection)
//
//    videoGamesCollection["Strategy"]?.add("Command & Conquer")
//    videoGamesCollection["Shooter"] = mutableListOf("Doom")
//    for (key in videoGamesCollection.keys){
//        println(key)
//    }
//
//    for(value in videoGamesCollection.values){
//        println(value)
//    }
//
//    for ((key,value )in videoGamesCollection){
//        println("Video $key genre you own are: $value")
//    }

//    val animalList = mutableMapOf<String, String>()
//
//    animalList["Tung"] = "VN Dog"
//    animalList["Tom"] = "Anime Cat"
//    animalList["Jerry"] = "Anime Mice"
//    animalList.remove("Tung")
//    for ((key,value) in animalList){
//        println("Pet name $key I own are: $value")
//    }

    val userID = setOf("ID1","ID2","ID3","ID4","ID5")
    println(userID)
    println(userID.contains("ID3"))
    println("ID1" in userID)

    val mutableSetID = userID.toMutableSet()
    println(mutableSetID.add("ID0"))
    println(mutableSetID)

    println(mutableSetID.add("ID10"))
    println(mutableSetID)

    println(mutableSetID.remove("ID1"))

    for (userIDs in mutableSetID){
        println(userIDs)
    }

    val name = arrayOf("Vu","Trang","Tung","Duy")
    println(name)

    val uniqueNames = name.toSet()
    println(uniqueNames)

    val itemsOnDesk = listOf<String>("Mouse","Keyboard","Cellphone","Cup","Mic","Speaker","Key","Key","Cellphone","Cigarette")
    val uniqueItems = itemsOnDesk.toMutableSet()
    println(uniqueItems)
    uniqueItems.remove("Cup")
    for (itemList in uniqueItems){
        print(" $itemList")
    }

}