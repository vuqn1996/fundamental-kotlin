import java.security.cert.CertPathValidator
import javax.print.DocFlavor
fun validatePass(password: String) = password.length>=10
fun main(){
//    fun createRange(start: Int, end: Int, isHalfOpen: Boolean = false) =
//        if (isHalfOpen) {
//            start until end
//        } else {
//            start..end
//        }
//    fun createRange(start: Char, end: Char, isHalfOpen: Boolean = false) =
//        if (isHalfOpen) {
//            start until end
//        } else {
//            start..end
//        }
//
//    fun printRange(range: IntRange){
//        for (number in range) print("$number \t")
//        println()
//    }
//    fun printRange(range: CharRange){
//        for (number in range) print("$number \t")
//        println()
//    }
//    val closedRange = createRange(1,10,false)
//    printRange(closedRange)
//
//    val halfOpenRange = createRange(1,10 ,true)
//    printRange(halfOpenRange)
//
//    val usingNameArgument = createRange(isHalfOpen = true,start = 10,end = 15)
//    printRange(usingNameArgument)
//
//    printRange(createRange(3,5,true))
//
//    val charRange = createRange('A','Z')
//    printRange(charRange)
//
//    fun parseCoordinates(input : String) : Pair<Double, Double>{
//        val  latitudeLongitude = input.split(",")
//        val latitude = latitudeLongitude[0].toDouble()
//        val longitude = latitudeLongitude[1].toDouble()
//
//        return latitude to longitude
//    }
//
//    val (latitude,longitude) = parseCoordinates("42.4,44.2")
//    println("The latitude is : $latitude, the longitude is: $longitude")

//    fun fullNameLength(name:String, lastName:String = "") = name.length + lastName.length
//
//    fun fullNameLength(name:String,midName:List<String>, lastName:String = ""): Int {
//        val allNameLength = name.length + lastName.length
//        var middleLength = 0
//        for (middleName in midName){
//            middleLength += middleName.length
//        }
//        return middleLength + allNameLength
//    }
//
//    val nameLength = fullNameLength("Vu", "Nguyen")
//    println(nameLength)
//
//    val length = fullNameLength("Vu", listOf("Hai","Pro"),"Nguyen")
//    println(length)

    val email: String? = "email@gmail.com"
    val password:String? = "password"
    fun validate(input:String?,inputType:String) =
        if (input == null || input.isEmpty()){
            false
        }else if (inputType == "Password"){
            input.length >=10
        }else if (inputType == "Email"){
            input.contains("@")
        }else{
            println("Cannot verify")
            false
        }
    fun validateString(input:String?,validator:(String) -> Boolean) =
        if(input == null || input.isBlank()){
            false
        }else{
            validator(input)
        }

    validate("Hello word","Welcome")

    val isValidEmail = validate(email, "Email")
    println(isValidEmail)

    val isValidPassword = validateString(password, ::validatePass)
    println(isValidPassword)

    val passwordValidator = ::validatePass
    println(passwordValidator)

    val validator: (String?) -> Boolean = {
            input -> input != null && validatePass(input)
    }
    println(validator)
}