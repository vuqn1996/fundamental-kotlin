import kotlin.math.roundToInt
import kotlin.math.sqrt

class Person(val firstName: String, val lastName: String) {
    val fullName = "$firstName $lastName"
}

class TVMath {
    companion object {
        fun getDiagonal(width: Double, height: Double): Int {
            val result = sqrt(width * width + height * height)
            return result.roundToInt()
        }

        fun getWitdhAndHeight(diagonal: Int, ratioWidth: Double, ratioHeight: Double): Pair<Double, Double> {
            val ratioDiagonal = sqrt(ratioWidth * ratioWidth + ratioHeight * ratioHeight)
            val height = diagonal * ratioHeight / ratioDiagonal
            val width = height * ratioWidth / ratioHeight

            return Pair(width, height)
        }
    }
}

class TV(var width: Double, var height: Double) {
    var diagonal: Int
        get() {
            return TVMath.getDiagonal(width, height)
        }
        set(value) {
            val size = TVMath.getWitdhAndHeight(
                diagonal = value,
                ratioWidth =  16.0,
                ratioHeight = 9.0
            )
            width = size.first
            height = size.second
        }
}

class Level(
    val id: Int,
    val boss: String,
    val unlocked: Boolean
) {
    companion object {
        var highestLevel = 1
    }

}

fun main() {
    val level1 = Level(1, "Outside Cat", true)
    val level2 = Level(2, "Laser Pointer", false)
    val level3 = Level(3, "Mysterious Sound", false)
    val level4 = Level(4, "Vacuum", false)

    println("Highest level is ${Level.highestLevel}")

    val vu = Person("Hai", "Vu")
    println(vu.fullName)

    val tv = TV(width = 95.22, height = 35.99)
    println(tv.diagonal)

    tv.width = tv.height
    println(tv.diagonal)

    tv.diagonal = 64
    println("That will be ${tv.width} inches")

}