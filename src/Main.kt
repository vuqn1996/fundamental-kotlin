fun main(arguments: Array<String>) {
    val isProgramGood = true
    println("Is programing good? $isProgramGood")
    val myAge = 23

    val isLegalDrivingAge = myAge >= 18
    println(isLegalDrivingAge)

    val ageTim = 27
    val isTimOlder = ageTim > myAge
    println(isTimOlder)

    val yearOfBirth = 1996

    val isBorn20Cent = yearOfBirth < 2000
    println(isBorn20Cent)

    val strangerName = "Duy"
    val myName = "Vu"
    val isSame = strangerName == myName
    println(!isSame)

    val areDif = myName != strangerName
    println(areDif)
    println(!areDif)

    val firstObjects = Any()
    val secondObjects = Any()

    println(firstObjects === secondObjects)
    println(firstObjects !== secondObjects)

    val isNameEmpty = myName.isEmpty()
    println(isNameEmpty)

    val password = "mothaibabonnam"
    val isSecure = password.length >= 10

    println(isSecure)

    val isSameLenght = strangerName.length == myName.length

    println(isSameLenght)
}