//class Pet(var name:String,val animalType:String)
//class Person(private val name: String,
//             private val lastName : String,
//             private var age: Int,
//             val pet:Pet){
//    fun setAge(age: Int){
//        if (age in 0..150){
//            this.age = age
//        }else{
//
//        }
//    }
//    fun getAge() = age
//
//    fun getFullInfor(): String{
//        return "Full name: $lastName $name,Age: $age, Pet: ${pet.name}, ${pet.animalType}"
//    }
//}

//fun main(){
//    val me = Person("Vu","Nguyen",25)
//    println("Name: ${me.name}, last name: ${me.lastName}, Age: ${me.age}")
//
//    val love = Person("Trang","Vu",23)
//    println("Name: ${love.name}, last name: ${love.lastName}, Age: ${love.age}")
//    val dog = Pet("Tung", "VN DOG")
//    val me = Person("Vu","Nguyen",25,dog)
//
//    println(me.getFullInfor())
//
//    me.setAge(28)
//    println(me.getAge())
//
//    println(me.pet.name)
//}

//class Movies(var name :String,
//             val genre : String,
//             var length : Int){
//    fun getFullInfor() :String{
//        return "Movies is $name, genre $genre, length $length minutes"
//    }
//    fun changeName(name: String){
//        this.name = name
//    }
//    fun changeLength(length: Int){
//        this.length = length
//    }
//}
//fun main(){
//    val movie1 = Movies("Iron Man", "Action", 120)
//    val movie2 = Movies("Captain America", "Action", 121)
//    val movie3 = Movies("Avengers: Endgame", "Action", 180)
//    val movie4 = Movies("Justice League", "Action",120)
//
//    println(movie1.getFullInfor())
//    println(movie2.getFullInfor())
//    println(movie3.getFullInfor())
//    println(movie4.getFullInfor())
//
//    movie4.changeName("Jack Snyder: Justice League")
//    movie4.changeLength(240)
//    println(movie4.getFullInfor())
//}

//data class Pet(var name: String, val animalType: String)
//data class Person(
//    val name: String,
//    val lastName: String,
//    var age: Int,
//    val pet: Pet
//) {
//}
//
//fun main() {
//    val dog = Pet("Tung", "VN DOG")
//    val me = Person("Vu", "Nguyen", 25, dog)
//    println(me)
//
//    val olderMe = me.copy(age = me.age + 5)
//    println(olderMe)
//    val (name, _, age, pet) = olderMe
//    println(pet)
//
//    val firstName = olderMe.component1()
//    println(firstName)
//
//    var me2 = me
//    println(me2 == me)
//    println(me2 === me)
//
//    me2 = me.copy()
//    println(me2 == me)
//    println(me2 === me)
//}

data class Movies(var name :String,
             val genre : String,
             var length : Int)
fun main(){
    val movie1 = Movies("Iron Man", "Action", 120)
    val movie2 = Movies("Captain America", "Action", 121)
    val movie3 = Movies("Avengers: Endgame", "Action", 180)
    val movie4 = Movies("Justice League", "Action",120)

    println(movie1)
    println(movie2)
    println(movie3)
    println(movie4)

    val movie4Change = movie4.copy(name = "Jack Snyder: Justice League", length = movie4.length + 120)
    println(movie4Change)
    println(movie4Change == movie4)
    println(movie4Change === movie4)

    val(name,genre,length) = movie4Change
    println("$name is a famous movies in 2021, it genre $genre and has length $length")
}