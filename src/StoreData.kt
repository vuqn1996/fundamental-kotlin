fun main(){
//    val birthday = Triple(3,9,1996)
//    val person = Pair("Hai Vu",birthday)
//    val nameUsingOrdering = person.first
//    val birthdayUsingOrdering = person.second
//
//    val (name,bday) = person
//    val (day,month,year) = bday
//    println("$name was born in $day/$month year $year")
//
//    val creditCard = Triple(12210001733732,391996,"Visa")
//    val bankAccount = Pair(creditCard,980000)
//    val accountUsing = bankAccount.second
//    val cardUsing = bankAccount.first
//
//    val (account,balance) = bankAccount
//    val (cardNum,securityCode,cardType) = account
//
//    println("My Bank account is $cardNum,type $cardType,balance is $balance")
//    val visitedCountries = arrayOf<String>(
//        "VN","HN", "HL","QN"
//    )
//    println(visitedCountries[3])
//    val countries = listOf("Russia","VN","USA")
//    println(countries)
//
//    val mutableList = countries.toMutableList()
//
//    mutableList.add("Germany")
//    mutableList.add(1,"Roman")
//    println(mutableList)
//    mutableList.addAll(3, listOf("Australia","Netherland"))
//    println(mutableList)
//    val hasBeenToJapan = "Japan" in mutableList
//    println(hasBeenToJapan)
//    mutableList.removeAt(3)
//    mutableList.remove("Roman")
//    mutableList.removeAll(listOf("Russia","Germany"))
//
//    println(mutableList)
//    val combinedList = countries + mutableList
//    val emptyList = emptyList<String>()
//    println(mutableList)
//    println(combinedList)
//    println(emptyList)
//
//    mutableList.clear()
//    println(mutableList)
//    val gameList = arrayOf("LoL","PoE","GTAV","MU","PES2021")
//    println(gameList)
//    println(gameList[2])
//    val mutableList = gameList.toMutableList()
//    mutableList.addAll(listOf("The Witcher 3","CoD"))
//    println(mutableList)
//    mutableList.remove("GTAV")
//    println(mutableList)
    val days = arrayOf("Monday","Tuesday","Wednesday","Thursday"
        ,"Friday","Saturday","Sunday")
    days[4] = "Thu 3"
    val daysList = days.toMutableList()
    println(daysList)
    daysList.remove("Monday")
    daysList.add(1,"Thu 2")
    println(daysList)
    println("Monday" in daysList)
}