//import java.awt.desktop.OpenFilesEvent
/*
Function Preview
* **/
//fun main(){
//    fun printRange(){
//        for (number in 1..5) print("$number \t")
//        println()
//    }
//
//    printRange()
//
//    fun printRange(range: IntRange){
//        for (number in range) print("$number \t")
//        println()
//    }
//    printRange(6..10)
//
//    fun createRange(start: Int, end : Int, isHalfOpen: Boolean): IntRange{
//        return if (isHalfOpen){
//            start until end
//        }else {
//            start..end
//        }
//    }
//    val halfOpenRange = createRange(11,15,true)
//    printRange(halfOpenRange)
//
//    val nameArguments = createRange(isHalfOpen = true,start = 100,end = 115)
//    printRange(nameArguments)
//    printRange(createRange(100,end =101,false))
//    fun creatRange(start: Char, end : Char, isHalfOpen: Boolean = false) =
//        if (isHalfOpen) start until end else start..end
//    fun printRange(range:CharRange){
//        for (char in range) print("$char \t")
//        println()
//    }
//
//    val closedRange = creatRange('A','G')
//    printRange(closedRange)
//}

/**
 * Lambdas Preview*/

//fun main() {
//    fun operateOnNumbers(a: Int, b: Int, operation: (Int, Int) -> Int): Int {
//        return operation(a, b)
//    }
//    val addLambda = {a:Int, b : Int ->
//        a +b
//    }
//
//    operateOnNumbers(4,2,operation = {
//        a,b-> a + b
///   })
///   fun addFunction(a: Int,b: Int) = a + b
//    operateOnNumbers(4,2,operation = Int::plus)
//    print(addFunction(4,2))
//}
//fun main(){
//    var counter = 0
//    val incrementCounter = {
//        counter +=1
//    }
//    incrementCounter()
//    incrementCounter()
//    incrementCounter()
//    incrementCounter()
//    incrementCounter()
//    println(counter)
//
//    fun countingLambda():() -> Int{
//        var counter = 0
//        val incrementCounter: () -> Int = {
//            counter += 1
//            counter
//        }
//        return incrementCounter
//    }
//    val counter1 = countingLambda()
//    val counter2 = countingLambda()
//
//    println(counter1())
//    println(counter1())
//
//    println(counter2())
//    println(counter2())
//    println(counter2())
//}
//fun main(){
//    var prices= arrayOf(1.50,10.00,4.99,2.30,8.19)
////    for (price in prices){
////        println(price)
////    }
//// forEch - Loop over a collection nad performs an operation
//    prices.forEach {
//        println(it) }
//    prices.forEach(::println)
//// map - Loop over a collection, executes lambda code, return new collection
////    var salePrices = mutableListOf<Double>()
////    for (price in prices){
////        salePrices.add(0.9 * price)
////    }
//    val salePrices = prices.map {it * 0.9 }
//    println(salePrices)
//    val userInput = listOf("LoL","15","20","Two","100")
//
//    val numbers = userInput.map{ it.toIntOrNull()}
//    println(numbers)
//
//    val validInput = userInput.mapNotNull { it.toIntOrNull() }
//    println(validInput)
//}
// P01E05 Challenge forEach & map - Starter

//// ----------------Starter Code----------------------
//class Student(val name: String, var grade: Int, var pet: String?, var libraryBooks: List<String>) {
//    fun getPassStatus(lowestPass: Int = 50) {
//        grade >= lowestPass
//    }
//
//    fun earnExtraCredit() {
//        grade += 10
//    }
//}
//
//val chris = Student(name = "Chris", grade = 49, pet = "Mango",
//    libraryBooks = listOf("The Book of Atrus", "Living by the Code", "Mastering Git"))
//val sam = Student(name = "Sam", grade = 99, pet = null,
//    libraryBooks = listOf("Mastering Git"))
//val catie = Student(name = "Catie", grade = 75, pet = "Ozma",
//    libraryBooks = listOf("Hogfather", "Good Omens"))
//val andrea = Student(name = "Andrea", grade = 88, pet = "Kitten",
//    libraryBooks = listOf("Dare to Lead", "The Warrior's Apprentice"))
//
//val students = arrayOf(chris, sam, catie, andrea)
////// ----------------Starter Code----------------------
//
//fun main() {
//
///*:
// Challenge 1 - `forEach` & `map`
// There are two `for` loops below.
// Rewrite one of them using `forEach` and the other with `map`
//*/
//
//    students.forEach{
//        println("Old grade: ${it.grade}")
//        it.earnExtraCredit()
//        println("New grade: ${it.grade}")
//
//    }
//
//    val classLibraryBooks: MutableList<List<String>> = mutableListOf()
//    for (student in students) {
//        classLibraryBooks.add(student.libraryBooks)
//    }
//    val classLibraryBooks2 = students.map { student ->
//        student.libraryBooks  }
//    println(classLibraryBooks2)
//
//
///*:
//  Challenge 2 - mapNotNull
//  Create a list of student pets
//*/
//    val classPets = students.mapNotNull { it.pet }
//    println(classPets)
//}
fun main() {
    /* See Dwarves below. I want them organized like this:
      - Grouped by which names come before or after M in the alphabet.
      - Sorted by name length.
      - No dwarves with names less than four letters long.
      - Turn dwarves before M into a map, grouped by the first letters in their names
     */

    // --------------------------------------
    val lotsOfDwarves = listOf(
        listOf("Sleepy", "Grumpy", "Doc", "Bashful", "Sneezy"),
        listOf("Thorin", "Nori", "Bombur")
    )
// --------------------------------------
    val (beforeM,mAndAfter) = lotsOfDwarves.flatMap { dwarves ->
        dwarves.filter { it.length > 3 }
    }.sortedBy { -it.length }.partition { it < "M" }
    val groupBeforeM = beforeM.groupBy { it.first() }
    println(groupBeforeM)
    println(mAndAfter)
}
